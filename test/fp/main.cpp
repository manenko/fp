// Copyright 2019 Oleksandr Manenko
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <fp/fp.hpp>
#include <iostream>
#include <optional>
#include <string>

namespace fp {

template<>
class Semigroup<std::string>
{
  public:
    inline static std::string concat(const std::string& x, const std::string& y)
    {
        return x + y;
    }
};

template<>
class Monoid<std::string> : public Semigroup<std::string>
{
  public:
    inline static std::string empty() { return std::string{}; }
};

template<>
class Functor<std::optional>
{
  public:
    template<typename A, typename B>
    inline static std::optional<B> map(std::function<B(const A&)> f,
                                       const std::optional<A>& fa)
    {
        if (fa.has_value()) {
            return std::optional{ f(fa.value()) };
        } else {
            return std::optional<B>{};
        }
    }
};

} // namespace fp

int
main()
{
    std::cout << fp::mconcat(std::string("Hello, "), std::string("world!"))
              << std::endl;

    auto x = std::make_optional(42);
    std::function<std::string(const int&)> stringify = [](const int& n) {
        return std::to_string(n);
    };
    auto y = fp::fmap(stringify, x);

    std::cout << y.value_or("None") << std::endl;
}
