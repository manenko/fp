#pragma once

// Copyright 2019 Oleksandr Manenko
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <functional>

namespace fp {

//! Type `A` forms a `semigroup` if it has an associative binary operation.
template<typename A>
class Semigroup
{
  public:
    //! If a type A can form a Semigroup it has an associative binary operation
    //! (`sconcat` in Haskell, `combine` in Scala Cats.).
    static A concat(const A& x, const A& b);

    Semigroup() = delete;
};

template<typename A>
class Monoid : public Semigroup<A>
{
  public:
    static A empty();
};

template<template<typename> typename F>
class Functor
{
    template<typename A, typename B>
    static F<B> map(std::function<B(const A&)> f, const F<A>& fa);
};

template<typename A>
inline constexpr A
sconcat(const A& x, const A& b)
{
    return Semigroup<A>::concat(x, b);
}

template<typename A>
inline constexpr A
mconcat(const A& x, const A& b)
{
    return Monoid<A>::concat(x, b);
}

template<typename A>
inline constexpr A
mempty()
{
    return Monoid<A>::mempty();
}

template<template<typename> typename F, typename A, typename B>
inline constexpr F<B>
fmap(std::function<B(const A&)> f, const F<A>& fa)
{
    return Functor<F>::map(f, fa);
}

} // namespace fp
